brocodeArticles = [
	{
		number : 1,
		caption : "Bros before ho's",
		description : "The bond between two men is stronger than the bond between a man and a woman because, on average, men are stronger than women. That.s just science."
	},
	{
		number : 2,
		caption : "A Bro is always entitled to do something stupid, as long as the rest of his Bros are all doing it.",
		description : ""
	},
	{
		number : 3,
		caption : "If a Bro gets a dog, it must be at least as tall as his knee when full-grown.",
		description : ""
	},
	{
		number : 4,
		caption : "A Bro never divulges the existence of the Bro Code to a woman. It is a sacred document not to be shared with chicks for any reason ... no, not even that reason.",
		description : ""
	},
	{
		number : 5,
		caption : "Whether he cares about sports or not, a Bro cares about sports.",
		description : ""
	},
	{
		number : 6,
		caption : "A Bro shall not lollygag if he must get naked in front of other Bros in a gym locker room.",
		description : ""
	},
	{
		number : 7,
		caption : "A Bro never admits he can't drive stick. Even after an accident.",
		description : ""
	},
	{
		number : 8,
		caption : "A Bro never sends a greeting card to another Bro.",
		description : ""
	},
	{
		number : 9,
		caption : "Should a Bro lose a body part due to an accident or illness, his fellow Bros will not make lame jokes such as \"Gimme three!\" or \"Wow, quitting your job like that really took a lot of ball.\" It's still a high five and that Bro still has a lot of balls ... metaphorically speaking, of course.",
		description : ""
	},
	{
		number : 10,
		caption : "A Bro will drop whatever he's doing and rush to help his Bro dump a chick.",
		description : ""
	},
	{
		number : 11,
		caption : "A Bro may ask his Bro(s) to help him move, but only after first disclosing an honest estimate on both time commitment and number of large pieces of furniture. If the Bro has vastly underestimated either, his Bros retain the right to leave his possessions where they are - in most cases, stuck in a doorway.",
		description : ""
	},
	{
		number : 12,
		caption : "Bros do not share dessert.",
		description : ""
	},
	{
		number : 13,
		caption : "All Bros shall dub one of their Bros his wingman.",
		description : ""
	},
	{
		number : 14,
		caption : "If a chick inquires about another Bro's sexual history, a Bro shall honor the Brode of Silence and play dumb. Better to have women think all men are stupid than to tell the truth.",
		description : ""
	},
	{
		number : 15,
		caption : "A Bro never dances with his hands above his head.",
		description : ""
	},
	{
		number : 16,
		caption : "A Bro should be able, at any time, to recite the following reigning champions: Super Bowl, World Series, and Playmate of the Year.",
		description : ""
	},
	{
		number : 17,
		caption : "A Bro shall be kind and courteous to his co-workers, unless they are beneath him on the Pyramid of Screaming.",
		description : ""
	},
	{
		number : 18,
		caption : "If a Bro spearheads a beer run at a party, he is entitled to any excess monies accrued after canvassing the group.",
		description : ""
	},
	{
		number : 19,
		caption : "A Bro shall not sleep with another Bro's sister. However, a Bro shall not get angry if another Bro says, \"Dude, your sister's hot!\"",
		description : ""
	},
	{
		number : 20,
		caption : "A Bro respects his Bros in the military because they've selflessly chosen to defend the nation, but more to the point, because they can kick his ass six ways to Sunday.",
		description : ""
	},
	{
		number : 21,
		caption : "A Bro never shares observations about another Bro's smoking-hot girlfriend. Even if the Bro with the hot girlfriend attempts to bait the Bro by saying, \"She's smoking-hot, huh?\" a Bro shall remain silent, because in this situation, he's the only one who should be baiting.",
		description : ""
	},
	{
		number : 22,
		caption : "There is no law that prohibits a woman from being a Bro.",
		description : ""
	},
	{
		number : 23,
		caption : "When flipping through TV channels with his Bros, a Bro is not allowed to skip past a program featuring boobs. This includes, but is not limited to, exercise shows, women's athletics, and on some occasions, surgery programs.",
		description : ""
	},
	{
		number : 24,
		caption : "When wearing a baseball cap, a Bro may position the brim at either 12 or 6 o'clock. All other angles are reserved for rappers and the handicapped.",
		description : ""
	},
	{
		number : 25,
		caption : "A Bro doesn't let another Bro get a tattoo, particularly a tattoo of a girl's name.",
		description : ""
	},
	{
		number : 26,
		caption : "Unless he has children, a Bro shall not wear his cell phone on a belt clip.",
		description : ""
	},
	{
		number : 27,
		caption : "A Bro never removes his shirt in front of other Bros, unless at a resort pool or the beach.",
		description : ""
	},
	{
		number : 28,
		caption : "A Bro will, in a timely manner, alert his Bro to the existence of a girl fight.",
		description : ""
	},
	{
		number : 29,
		caption : "If two Bros decide to catch a movie together, they may not attend a screening that begins after 4:40pm. Also, despite the cost savings, they shall not split a tub of popcorn, choosing instead to procure individuals bags.",
		description : ""
	},
	{
		number : 30,
		caption : "A Bro doesn't comparison shop.",
		description : ""
	},
	{
		number : 31,
		caption : "When on the prowl, a Bro hits on the hottest chick first because you just never know.",
		description : ""
	},
	{
		number : 32,
		caption : "A Bro doesn't allow another Bro to get married until he's at least thirty.",
		description : ""
	},
	{
		number : 33,
		caption : "When in a public restroom, a Bro (1) stares straight ahead when using the urinal; (2) makes the obligatory comment, \"What is this, a chicks' restroom?\" if there are more than two dudes waiting to pee; and (3) attempts to shoot his used paper towel into the trash can like a basketball ... rebounding is optional.",
		description : ""
	},
	{
		number : 34,
		caption : "Bros cannot make eye contact during a devil's threeway.",
		description : ""
	},
	{
		number : 35,
		caption : "A Bro never rents a chick flick.",
		description : ""
	},
	{
		number : 36,
		caption : "When questioned in the company of women, a Bro always decries fake breasts.",
		description : ""
	},
	{
		number : 37,
		caption : "A Bro is under no obligation to open a door for anyone. If women insist on having their own professional basketball league, then they can open their own doors. Honestly, they're not that heavy.",
		description : ""
	},
	{
		number : 38,
		caption : "Even in a fight to the death, a Bro never punches another Bro in the groin.",
		description : ""
	},
	{
		number : 39,
		caption : "When a Bro gets a chick's number, he waits at least ninety-six hours before calling her.",
		description : ""
	},
	{
		number : 40,
		caption : "Should a Bro beomce stricken with engagement, his Bro shall stage an intervention and attempt to heal him. This is more commonly known as \"a bachelory party.\"",
		description : ""
	},
	{
		number : 41,
		caption : "A Bro never cries.",
		description : "EXCEPTIONS: Watching Field of Dreams, E.T., or a sports legend retire."
	},
	{
		number : 42,
		caption : "Upon greeting another Bro, a Bro may engage in a high five, fist bump, or Bro hug, but never a full embrace.",
		description : ""
	},
	{
		number : 43,
		caption : "A Bro loves his country, unless that country isn't America.",
		description : ""
	},
	{
		number : 44,
		caption : "A Bro never applies sunscreen to another Bro.",
		description : "EXCEPTION: If the Bros are within 7 degrees latitude of the equator."
	},
	{
		number : 45,
		caption : "A Bro never wears jeans to a strip club.",
		description : ""
	},
	{
		number : 46,
		caption : "If a Bro is seated next to some dude who's stuck in the middle seat on an airplane, he shall yield him all of their shared armrest, unless the dud has (a) taken his shoes off, (b) is snoring, (c) makes the Bro get up more than once to use the lavatory, or (d) purchased headphones after they announced the in-flight movie is 27 Dresses. See Article 35.",
		description : ""
	},
	{
		number : 47,
		caption : "A bro never wears pink. Not even in Europe.",
		description : ""
	},
	{
		number : 48,
		caption : "A Bro never publicly reveals how many chicks he's banged.",
		description : "COROLLARY: A Bro also never reveals how many chicks another Bro has banged."
	},
	{
		number : 49,
		caption : "When asked, \"Do you need some help?\" a Bro shall automatically respond, \"I got it,\" whether or not he's actually got it.",
		description : ""
	},
	{
		number : 50,
		caption : "If a Bro should accidentally strike another Bro's undercarriage with his arm while walking, both Bros silently agree to continue on as if it never happend.",
		description : ""
	},
	{
		number : 51,
		caption : "A Bro checks out another Bro's blind date and reports back with a thumbs-up or thumbs-down.",
		description : ""
	},
	{
		number : 52,
		caption : "A Bro is not required to remember another Bro's birthday, though a phone call every now and again probably wouldn't kill him.",
		description : ""
	},
	{
		number : 53,
		caption : "Even in a drought, a Bro flushes twice.",
		description : ""
	},
	{
		number : 54,
		caption : "A Bro is required to go out with his Bros on St. Patty's Day and other official Bro holidays, including Halloween, New Year's Eve and Desperation Day (February 13).",
		description : ""
	},
	{
		number : 55,
		caption : "Even in an emergency that requires a tourniquet, a Bro never borrows from or lends clothes to another Bro.",
		description : ""
	},
	{
		number : 56,
		caption : "A Bro is required to alert another Bro if the Bro/Chick Ratio at a party falls below 1:1. However, to avoid Broflation, a Bro is only allowed to alert one Bro. Further, a Bro may not speculate on the anticipated Bro/Chick Ratio of a party or venue without first disclosing the present-time observed ratio.",
		description : ""
	},
	{
		number : 57,
		caption : "A Bro never reveals the score of a sporting event to another Bro unless that Bro has thrice confirmed he wants to hear it.",
		description : ""
	},
	{
		number : 58,
		caption : "A Bro doesn't grow a mustache.",
		description : "EXCEPTION: When shaving, it's more than okey for a Bro to keep the whiskers aorund his mouth until the end so that he might temporarily experiment with different facial hair configurations."
	},
	{
		number : 59,
		caption : "A Bro must always post bail for another Bro, unless it's out ouf state or, like, crazy expensive.",
		description : "When is bail crazy expensive? Crazy Expensive Bail > (Years you've been Bros) * $100"
	},
	{
		number : 60,
		caption : "A Bro shall honor thy father and mother, for they were once Bro and chick. However, a Bro never thinks of them in that capacity.",
		description : ""
	},
	{
		number : 61,
		caption : "If a Bro, for whatever reason, becomes aware of another Bro's anniversary with a chick, he shall endeavor to make that information available to his Bro, regardless of whether he thinks his Bro already knows.",
		description : ""
	},
	{
		number : 62,
		caption : "In the event that two Bros lock on to the same target, the Bro who calls dib first has dibs. If both call dibs at the same time, the Bro who counts aloud to ten the fastest has dibs. If both arrive at the number ten at the same time, the Bro who bought the last round of drinks has dibs. If they haven't purchased drinks yet, the taller of the two Bros has dibs. If they're the same height, the Bro with the longer dry spell has dibs. Should the dry spell be of equal length, a game of discreet Broshambo shall determine dibs, provided the chick is still there.",
		description : "Broshambo = Rock, paper, scissors for Bros"
	},
	{
		number : 63,
		caption : "A Bro will make any and all efforts to provide his Bro with protection.",
		description : ""
	},
	{
		number : 64,
		caption : "A Bro must provide his Bro with a ticket to an event if said event involves the latter Bro's favorite sports team in a playoff scenario.",
		description : ""
	},
	{
		number : 65,
		caption : "A Bro must always reciprocate a round of drinks among Bros.",
		description : ""
	},
	{
		number : 66,
		caption : "If a Bro suffers pain due to the permanent dissolution of a relationship with a lady friend, his Bros shall offer no more than a \"that sucks, man\" and copious quantities of beer. To eliminate the possibility of any awkward moments in the future, his Bros shall also refrain from any pejorative commentary -- deserved or not -- regarding said lady friend for a period of three months, when the requisite backslide window has fully closed.",
		description : ""
	},
	{
		number : 67,
		caption : "Should a Bro pick up a guitar at a party and commence playing, another Bro shall point out that he is a tool.",
		description : ""
	},
	{
		number : 68,
		caption : "If a Bro be on a hot streak, another Bro will do everything possible to ensure its longevity, even if that includes jeopardizing his own personal records, the missing of work, or, if necassary, generating a realistic fear that the end of the world is imminent.",
		description : "EXCEPTION: Dry spell trumps hot streak."
	},
	{
		number : 69,
		caption : "Dub.",
		description : ""
	},
	{
			number : 70,
			caption : "A Bro will drive another Bro to the airport or pick him up, but never both for the same trip. He is not expected to be on time, help with luggage, or inquire about his Bro's trip or general well-being.",
			description : ""
	},
	{
			number : 71,
			caption : "As a courtesy to Bros the world over, a Bro never brings more than two other Bros to a party.",
			description : ""
	},
	{
			number : 72,
			caption : "A Bro never spell-checks.",
			description : ""
	},
	{
			number : 73,
			caption : "When a group of Bros are in a restaurant, each shall engage in the time-honored ritual of jockeying to pay the bill, regardless of affordibility. When the group ultimately decides to divide the check, each Bro shall act upset rather than enormously relieved.",
			description : ""
	},
	{
			number : 74,
			caption : "At a red light, a Bro inches as close as possible to the rear bumper of the car in front of him, and then immediately honks his horn when the light turns green. That way, if another Bro is several cars behind, he'll have a better chance of making it through the intersection before the light turns red again.",
			description : ""
	},
	{
			number : 75,
			caption : "A Bro automatically enhances another Bro's job description when introducing him to a chick.",
			description : ""
	},
	{
			number : 76,
			caption : "If a Bro is on the phone with a chick while in front of his Bros and, for whatever reason, desires to say \"I love you,\" he shall first excuse himself from the room or employ a subsonic, Barry White-esque tone.",
			description : ""
	},
	{
			number : 77,
			caption : "Bros don't cuddle.",
			description : "-EXCEPTION: To conserve body heat in an emergency situation.-"
	},
	{
			number : 78,
			caption : "A Bro shall never rack jack his wingman.",
			description : ""
	},
	{
			number : 79,
			caption : "At a wedding, Bros shall reluctantly trudge out for the garter toss and feign interest for the benefigt of the chicks present. Whichever Bro gets stuck with the garter shall lightheartedly pretend he's not horrified at the thought of being the next one to drop before scurrying to the bar for a very stiff drink and/or shots.",
			description : ""
	},
	{
					number : 80,
					caption : "A Bro shall make every effort to aid another Bro in riding the tricyle, short of completing the tricycle himself.",
					description : "tricycle = Engaging in a threesome."
	},
	{
					number : 81,
					caption : "A Bro leaves the toilet seat up for his Bros.",
					description : ""
	},
	{
					number : 82,
					caption : "If two Bros get into a heated argument over something and one says something out of line, the other shall not expect him to \"take it back\" or \"apologie\" to make amends. That's inhuman.",
					description : ""
	},
	{
					number : 83,
					caption : "A Bro shall, at all costs, honor the Platinum Rule: Never, ever, ever, ever \"love\" thy neighbor. In particular, a Bro shall never mix it up romantically with a co-worker.",
					description : ""
	},
	{
					number : 84,
					caption : "A Bro shall stop whatever he's doing and watch Die Hard if it's on TV.",
					description : ""
	},
	{
					number : 85,
					caption : "If a Bro buys a new car, he is required to pop the hood when showing it off to his Bros.",
					description : ""
	},
	{
					number : 86,
					caption : "When a Bro meets a chick, he shall endeavor to find out where she fits on the Hot/Crazy Scale before pursuing her.",
					description : ""
	},
	{
					number : 87,
					caption : "A Bro never questions another Bro's stated golf score, maximum bench press, or height. He can, however, ask the Bro to prove it, traditionally in the form of a wager.",
					description : ""
	},
	{
					number : 88,
					caption : "If a Bro, for whatever reason, must drive another Bro's car, he shall not adjust the preprogrammed radio stations, the mirrors, or the seat position, even if this last requirement results in the Bro trying to drive the vehicle as a giant praying mantis would.",
					description : ""
	},
	{
					number : 89,
					caption : "A Bro shall always say yes in support of a Bro.",
					description : ""
	},
	{
					number : 90,
					caption : "A Bro shows up at another Bro's party with at least one more unit of alcohol than he plans to drink. So if a Bro plans on chugging a six-pack, he shall bring a six-pack plus at least one can of beer. If the party sucks and/or there are too many dudes, the Bro is entitled to leave with his alcohol, though etiquette dictates he should wait until nobody is looking.",
					description : ""
	},
	{
					number : 91,
					caption : "If a group of Bros suspect that their Bro is trying to give himself a nickname, they shall rally to call him by an adjacent yet more demeaning nickname.",
					description : ""
	},
	{
					number : 92,
					caption : "A Bro keeps his booty calls at a safe distance.",
					description : ""
	},
	{
					number : 93,
					caption : "Bros don't speak French to one another.",
					description : ""
	},
	{
					number : 94,
					caption : "If a Bro is in the bathroom and runs out of toilet paper, another Bro may toss him a new roll, but at no point may their hands touch, or the door open more than 30 degrees.",
					description : ""
	},
	{
					number : 95,
					caption : "A Bro shall alert another Bro to the presence of a chesty woman, regardless of whether or not he knows the Bro. Such alerts ma not be administered verbally.",
					description : ""
	},
	{
					number : 96,
					caption : "Bros shall go camping once a year, or at least attempt to start a fire.",
					description : "NOTE: Attempt to start a fire outside."
	},
	{
					number : 97,
					caption : "Where a Bro went to college is going to kick his Bro's college's ass all over the field this weekend.",
					description : ""
	},
	{
					number : 98,
					caption : "A Bro never lies to his Bros about the hotness of chicks at a given social venue or event.",
					description : ""
	},
	{
					number : 99,
					caption : "A Bro never asks for directions when lost.",
					description : ""
	},
	{
					number : 100,
					caption : "When pulling up to a stoplight, a Bro lowers his window so that all might enjoy his music selection.",
					description : ""
	},
	{
					number : 101,
					caption : "If a Bro asks another Bro to keep a scret, he shall take that secret to his grave. This is what makes them Bros, not chicks.",
					description : ""
	},
	{
					number : 102,
					caption : "A Bro shall take great care in selecting and training his wingman.",
					description : ""
	},
	{
					number : 103,
					caption : "A Bro never wears socks with sandals. He commits to one cohesive footgear plan and sticks with it.",
					description : ""
	},
	{
					number : 104,
					caption : "The mom of a Bro is always off-limits. But the stepmom of a Bro is fair game if she initiates and/or is wearing at least one article of leopard print clothing ... provided she looks good in it ... but not if she smokes menthol cigarettes.",
					description : ""
	},
	{
					number : 105,
					caption : "If a Bro is not invited to another Bro's wedding, he doesn't make a big deal out of it, even if, let's face it, he was kind of responsible for setting up the couple and had already picked out the perfect wedding gift and everything. It's cool. No big whoop.",
					description : ""
	},
	{
					number : 106,
					caption : "Given an option on quantity when ordering a beer with his Bros, a Bro always selecs the largets size available or shall never hear the end of it that night.",
					description : ""
	},
	{
					number : 107,
					caption : "A Bro never leaves another Bro hanging.",
					description : ""
	},
	{
					number : 108,
					caption : "If a Bro forgets a guy's name, he may call him \"brah,\" \"dude,\" or \"man,\" but never \"Bro.\"",
					description : ""
	},
	{
					number : 109,
					caption : "When Bros attend a sporting event and see themselves on the JumboTron, they shall purse their lips and flex their biceps while informing the crowd that their team is number one, despite any objective rankings to the contrary.",
					description : ""
	},
	{
					number : 110,
					caption : "If a Bro is hitting it off with a chick, his Bro shall do anything within his means to ensure the desired outcome.",
					description : ""
	},
	{
					number : 111,
					caption : "If a Bro discovers another Bro has forgotten to sign out of his email, the Bro will sign out for him, but only after first sending a few angry emails to random contacts and then deleting all sent messages.",
					description : ""
	},
	{
					number : 112,
					caption : "A Bro doesn't sing along to music in a bar.",
					description : "EXCEPTION: A Bro may participate in karaoke."
	},
	{
					number : 113,
					caption : "A Bro abides by the accepted age-difference formula when pursuing a younger chick.",
					description : ""
	},
	{
					number : 114,
					caption : "If a Bro must crash on his Bro's couch for an extended period of time, he shall offer to split the cost of toilet paper and the cable bill if said period exceeds two weeks. If he stays longer than a month, he shall offer to contribute some rent. If he stays longer than two months, he shall steam clean the couch or have it incinerated, whichever is more applicable.",
					description : ""
	},
	{
					number : 115,
					caption : "A \"clothing optional\" beach doesn't really mean \"clothing optional\" for Bros.",
					description : ""
	},
	{
					number : 116,
					caption : "A Bro shall not kill another Bro or a Bro's chance to score with a chick.",
					description : "Every Bro is endowed with a right to life and a right to pursue hot chicks. Violating either of these God-given rights is a heinous offense that could result in the strictest penalty recognized in the Bro Code: loss of permanent shotgun status."
	},
	{
					number : 117,
					caption : "A Bro never willingly relinquishes possession of a remote control. If another Bro desires a channel change, he may verbally request onre or engage in the fool's errand of getting up to manually change the channel.",
					description : "COROLARY: It is fully expected a Bro will try anything to gain posession of the remote, up to and including an attempt to flatulently smoke his Bro(s) out of the room."
	},
	{
					number : 118,
					caption : "When a Bro is with his Bros, he is not a vegetarian.",
					description : ""
	},
	{
					number : 119,
					caption : "When three Bros must share the backseat of a car, it is unacceptable for any Bro to put his arm around another Bro to increase space. Likewise, it is unacceptable for two Bros to share a motorcycle, unless said motorcycle is equipped with a sidecar ... a Brotorcycle.",
					description : ""
	},
	{
					number : 120,
					caption : "A Bro always calls another Bro by his last name.",
					description : "EXCEPTION: If a Bro's last name is also a racial epithet."
	},
	{
					number : 121,
					caption : "Even if he's never skied before, a Bro doesn't trifle with the bunny slope.",
					description : "COROLLARY: If a Bro experiences a catastrophic wipeout, he can always blame his bindings or the \"conditions.\""
	},
	{
					number : 122,
					caption : "A Bro is always psyched. Always.",
					description : ""
	},
	{
					number : 123,
					caption : "Two Bros shall maintain at least a three-foot radius between them while dancing on the same floor, even when reenacting the knife fight from \"Beat It,\" which, I guess, two Bros shouldn't do anyway, or at least not very often.",
					description : ""
	},
	{
					number : 124,
					caption : "If a Bro should shoot an air ball, strike out while playing softball, or throw a gutter ball while Browling, he is required to make some sort of excuse for himself.",
					description : ""
	},
	{
					number : 125,
					caption : "If a Bro is driving ahead of another Bro in a Bro Train, he is required to attempt to lose him in traffic as a funny joke.",
					description : ""
	},
	{
					number : 126,
					caption : "In a scenario where two or more Bros are watching entertainment of the adult variety, one Bro is forbbiden from intentionally or unintentionally touching another Bro in ANY capacity. This may include but is not limited to: the high five, the fist bump, or the congratulatory gluteal pat. Winking is also kind of a no-no.",
					description : ""
	},
	{
					number : 127,
					caption : "A Bro will always help another Bro reconstruct the events from the previous night, unless those events entail hooking up with an ugly chick or the Bro repeatedly saying \"I love you, man\" to all his Bros.",
					description : ""
	},
	{
					number : 128,
					caption : "A Bro never wears two articles of clothing at the same time that bear the same school name vacation destination, or sports team. Even in a laundry emergency, it's preferred that a Bro go out half naked rather thatn violate this code ... half naked from the waist up, naturally.",
					description : ""
	},
	{
					number : 129,
					caption : "If a Bro lends another Bro a DVD, video game, or piece of lawn machinery, he shall not expect to ever get it back, unless his Bro happens to die and bequeath it back to him.",
					description : ""
	},
	{
					number : 130,
					caption : "If a Bro learns another Bro has been in a traffic accident, he must first ask what type of car he collided with and whether it got totaled before asking if his Bro is okay.",
					description : ""
	},
	{
					number : 131,
					caption : "While a Bro is not expected to know exactly how to change a tire, he is required to at least drag out the jack and stare at the flat for a while. If he needs to consult the car's ownership manual to locate the jack, he shall do so from inside the car, where he can discreetly call a tow truck, after which it is recommended that he hide the jack by the side of the road so he'll have a legitimate excuse when the tow truck arrives",
					description : ""
	},
	{
					number : 132,
					caption : "If a Bro decides to let all of his Bro down and get married, he is required to invite them to the wedding, even if this directly violates the wishes of his fiancée and results in a \"no sex\" penalty or whatever lame domestic punishment couples might employ.",
					description : ""
	},
	{
					number : 133,
					caption : "A Bro only claims a fart after first accusing at least one other Bro.",
					description : "EXCEPTION: \"Pull my finger.\""
	},
	{
					number : 134,
					caption : "A Bro is entitled to use a woman as his wingman.",
					description : ""
	},
	{
					number : 135,
					caption : "If a scenario arises in which a Bro has promised two of his Bros permanent shotgun, one of the following shall determine the copilot: (1) foot race to the car, (2) silent auction, or in the case of a road trip exceeding 450 miles, (3) a no-holds-barred cage match to the death.",
					description : ""
	},
	{
					number : 136,
					caption : "When interrogated by a girlfriend about a bachelor party, a Bro shall offer nothing more than an uninterested \"It was okay.\"",
					description : ""
	},
	{
					number : 137,
					caption : "When hosting, a Bro orders enough pizza for all his Bros.",
					description : ""
	},
	{
					number : 138,
					caption : "A real Bro doesn't laugh when a guy gets hit in the groin.",
					description : "EXCEPTION: Unless he doesn't know the guy."
	},
	{
					number : 139,
					caption : "Regardless of veracity, a Bro never admits familiarity with a Broadway show or musical, despite the fact that, yes, \"Broadway\" begins with \"Bro.\"",
					description : ""
	},
	{
					number : 140,
					caption : "A Bro reserves the right to simply walk away during the first five minutes of a date.",
					description : "The Lemon Law"
	},
	{
					number : 141,
					caption : "A Bro can only get a manicure if (a) he's trying to sleep with the hot Asian woman performing the manicure, or (b) it's been longer than a month since his last manicure. It's called the Bro Code, not the Slob Code.",
					description : ""
	},
	{
					number : 142,
					caption : "A Bro shall seek no revenge if he passes out around his Bros and wakes up to find marker all over his face.",
					description : ""
	},
	{
					number : 143,
					caption : "When executing a high five, a Bro is forbidden from intertwining fingers or grasping his Bro's hand.",
					description : ""
	},
	{
					number : 144,
					caption : "It is unacceptable for two Bros to share a hotel bed without first exhausting all couch, cot, and pillows-on-floor combinations. If it's still unavoidable, they shall prevent any incidental spoonage by arm wrestling* to determine who sleeps under the covers. Once decided, each Bro shall don as many lower layers as possible before silently fist bumping the other good night.",
					description : "*Not on the bed."
	},
	{
					number : 145,
					caption : "A Bro is never offended if another Bro fails to return a phone call, text, or email in a timely fashion.",
					description : ""
	},
	{
					number : 146,
					caption : "A Bro refrains from using too much detail when relating sexual exploits to his Bros.",
					description : ""
	},
	{
					number : 147,
					caption : "If a Bro sees another Bro get into a fight, he immediately has his Bro's back.",
					description : ""
	},
	{
					number : 148,
					caption : "A Bro doesn't listen to chick music ... in front of other Bros. When alone, a Bro may listen to, say, a Sarah McLachlan album or two, but only to gain valuable insights into the female psyche, not because he finds her melodies tragically haunting yet curiously uplifting at the same time.",
					description : ""
	},
	{
					number : 149,
					caption : "A Bro pretends to understand and enjoy cigars.",
					description : ""
	},
	{
					number : 150,
					caption : "No sex with your Bro's ex.",
					description : "It is never, ever permissible for a Bro to sleep with his Bro's ex. Violating this code is worse than killing a Bro."
	}
];
