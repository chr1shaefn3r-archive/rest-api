require('./brocodearticles');

var mongo = require('mongodb');

var Server = mongo.Server,
	Db = mongo.Db,
	BSON = mongo.BSONPure;

var server = new Server(process.env[process.env.DATABASE_URL], 27017, {auto_reconnect: true}),
	db = new Db('brocodedb', server),
	brocodeCollection = null;

var TWEETMAXSIZE = 140,
	ARTICLESIZE = 5,
	HASHTAGSIZE = 9;
var TWEETSIZE = TWEETMAXSIZE - ARTICLESIZE - HASHTAGSIZE;

db.open(function(err, db) {
	if(err) throw(JSON.stringify(err));
	console.log("[brocodedao][*] Connected to 'brocodedb' database");
	//db.collection('brocode', {strict:true}, function(err, collection) {
	db.collection('brocode', function(err, collection) {
		if(err) throw(JSON.stringify(err));
		brocodeCollection = collection;
		brocodeCollection.drop();
		populateDB(brocodeCollection);
	});
});
var populateDB = function(collection) {
	collection.insert(brocodeArticles, {w:1}, function(err, result) {
		if(err) throw(JSON.stringify(err));
		console.log("[brocodedao][+] Successfully added brocodeArticles to DB");
	});
	collection.ensureIndex("number", function() {});
};

exports.getByNumber = function(number, callback) {
	brocodeCollection.findOne({number : parseInt(number) }, function(err, item) {
		callback(err, item);
	});
};

exports.getAll = function(callback) {
	brocodeCollection.find().toArray(function(err, items) {
		callback(err, items);
	});
};

exports.getAllWithWhereClause = function(where, callback) {
	brocodeCollection.find(where).toArray(function(err, items) {
		callback(err, items);
	});
};

exports.getAllTweetable = function(callback) {
	exports.getAllWithWhereClause({$where : "this.caption.length < "+TWEETSIZE}, callback);
};


exports.getAllTwitified = function(callback) {
	exports.getAll(function(err, items) {
		items = items.map(function(item) {
			item.caption = item.caption
				.replace(/because/g, "b/c")
				.replace(/without/g, "w/o")
				.replace(/with/g, "w/")
				.replace(/forward/g, "fwd");
			return item;
		});
		items = items.filter(function(item) {
			return item.caption.length < TWEETSIZE;
		});
		console.log("[brocodedao][+] length: "+items.length);
		callback(err, items);
	});
};


exports.getMax = function(callback) {
	db.command({ count: 'brocode' }, function(err, result) {
		callback(err, result);
	});
};

