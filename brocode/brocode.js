var dao = require('./brocodedao.js');

var articlePrefix = '/brocode/article/';
var routeMain = '/brocode',
	routeAll = '/brocode/all',
	routeByNumber = articlePrefix+':id',
	routeRandom = '/brocode/random',
	routeRandomTweetable = '/brocode/random/tweetable',
	routeRandomTweet = '/brocode/random/tweet/',
	routeRandomBotTweet = '/brocode/random/bottweet/';

exports.setup = function(app) {
	app.get(routeMain, main);
	app.get(routeAll, findAll);
	app.get(routeByNumber, findByNumber);
	app.get(routeRandom, random);
	app.get(routeRandomTweetable, randomTweetable);
	app.get(routeRandomTweet, randomTweet);
	app.get(routeRandomBotTweet, randomBotTweet);
};

exports.configure = function(rules) {
	// User request will be denied if "/brocode" path is accessed more
	// than 20 times in 120 seconds from the same IP address.
	rules.push([routeMain, 'all', 20, 120, true]);
};

var main = function(req, res) {
	var README = "Project: Brocode\r\n";
	README =  README.concat("Maintainer: Christoph Haefner\r\n");
	README =  README.concat("Version: 2.0\r\n");
	res.send(README);
};

var findAll = function(req, res) {
	dao.getAll(function(err, items) {
		res.send(items);
	});
};

var findByNumber = function(req, res) {
	var id = req.params.id;
	dao.getByNumber(id, function(err, item) {
		sendItem(res, item);
	});
};

var random = function(req, res) {
	dao.getMax(function(err, result) {
		var id = getRandomInt(1, result.n);
		dao.getByNumber(id, function(err, item) {
			sendItem(res, item);
		});
	});
};

var randomTweetable = function(req, res) {
	dao.getAllTweetable(function(err, result) {
		var id = getRandomInt(0, (result.length-1));
		sendItem(res, result[id]);
	});
};

var randomTweet = function(req, res) {
	dao.getAllTwitified(function(err, result) {
		var id = getRandomInt(0, (result.length-1));
		var item = result[id];
		res.send(item.number+": "+item.caption+" #brocode");
	});
};

var randomBotTweet = function(req, res) {
	dao.getAll(function(err, items) {
		res.send(createBotTweet(items));
	});
};
var createBotTweet = function(items) {
	var id = getRandomInt(0, (items.length-1));
	return craftTweetFromArticle(items[id]);
};
var craftTweetFromArticle = function(item) {
	var article = item.number+": ",
		link = " ĉĥ.de/brcd/#/"+item.number,
		hashtag = " #brocode",
		separator = "...";
	var TWEETMAXSIZE = 130,
		AVAILABLE = TWEETMAXSIZE - article.length - hashtag.length;
	if(item.caption.length < AVAILABLE) {
		return article+item.caption+hashtag;
	}
	var CAPTIONLENGTH = TWEETMAXSIZE - article.length - separator.length - link.length - hashtag.length;
	var shortCaption = item.caption.substr(0, CAPTIONLENGTH);
	return article+shortCaption+separator+link+hashtag;
};

var sendItem = function(res, item) {
	res.location(articlePrefix+item.number);
	res.send(item);
};

var getRandomInt = function(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
};

