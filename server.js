var express = require("express"),
	logger = require('morgan'),
	brocode = require('./brocode/brocode'),
	limiter = require('rate-limiter'),
	sprintf = require('sprintf');

var app = express();
app.use(logger('dev'));
app.use(allowCrossDomain);

var rules = [];
brocode.configure(rules);

app.get('/', function (req, res) {
	res.send('Hello World!');
});

brocode.setup(app);

var server = app.listen(process.env.REST_API_PORT, function () {
	var host = server.address().address;
	var port = server.address().port;

	console.log('Example app listening at http://%s:%s', host, port);
});

function errHandler(req, res, limit, ipLimitData) {
	var errMsg = sprintf('A limit of %d requests in %s seconds ' +
                     'has been reached. Request aborted.\n',
                     limit.request_count,
                     limit.request_period);
	res.send(429, errMsg);
}

function allowCrossDomain(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Methods", "GET, DELETE, POST");
	res.header("Access-Control-Allow-Headers", "Content-Type");
	next();
}

